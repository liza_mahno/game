import random

class Unit:
    def __init__(self, name = "Noname", health = 100, attack = 15, defense = 0):
        if not all([
            isinstance(name, str),
            isinstance(health, int),
            isinstance(attack, int),
            isinstance(defense, int)
        ]):
            raise TypeError
        self.name = name if name != "" else "Noname"
        self.health = health if health >= 0 else 100
        self.attack = attack if attack >= 0 else 15
        self.defense = defense if defense >= 0 else 0
        self.list_ammunition = []

    def __str__(self):
        return f"{self.name} - health {self.health}, attack {self.attack}, defense {self.defense}"

    def damage(self, enemy):
        if not isinstance(enemy, Unit):
            raise TypeError

        # Рандомный урон
        damage = enemy.defense - random.randint(self.attack - int(self.attack * 0.2), self.attack + int(self.attack * 0.2))
        if damage >= 0:
            return f"{self.name} не смог ранить {enemy.name}\n{enemy}"
        enemy.health += damage

        # Убийство противника
        if enemy.health <= 0:
            return f"{self.name} наносит критический урон {damage}\n{enemy.name} пал в бою..."

        # постепенно убавляет защиту врага
        if enemy.defense > 0:
            enemy.defense += int(damage * 0.2)
        if enemy.defense < 0:
            enemy.defense = 0

        return f"{self.name} наносит урон {damage}\n{enemy}"

    def put_on_gear(self, stuff):
        if not isinstance(stuff, Ammunition):
            raise TypeError
        self.health += stuff.health if self.health < 1000 else 0
        self.attack += stuff.attack if self.attack < 150 else 0
        self.defense += stuff.defense if self.defense < 150 else 0
        self.list_ammunition.append(stuff)
        return f"{self.name} надевает предмет амуниции - {stuff}\nХарактеристики {self.name} изменены - {self}"

class Ammunition:
    def __init__(self, name = "Noname", health = 0, attack = 0, defense = 0):
        if not all([
            isinstance(name, str),
            isinstance(health, int),
            isinstance(attack, int),
            isinstance(defense, int)
        ]):
            raise TypeError
        self.name = name if name != "" else "Noname"
        self.health = health
        self.attack = attack
        self.defense = defense

    def __str__(self):
        return f"{self.name} - health {self.health}, attack {self.attack}, defense {self.defense}"

# Создайте отдельную ветку, в ней реализуйте возможность изменения параметров юнитов во время битвы - например
# при создании битвы всем юнитам внутри битвы добавляется к атаке и к защите случайный коэффициент.
# После реализации функционала смержите эту ветку в основную

class Battle:
    def __init__(self, gamer1, gamer2, *args):
        if isinstance(gamer1, Unit) and isinstance(gamer2, Unit):
            self.gamer1 = gamer1
            self.gamer2 = gamer2

            self.list_ammunition = []
            for arg in args:
                if isinstance(arg, Ammunition):
                    self.list_ammunition.append(arg)
        else:
            raise TypeError

    def fight(self):

        if self.gamer1.health == 0:
            return f"Кажется {self.gamer1.name} не в состоянии начать бой... {self.gamer2.name} побеждает!"

        if self.gamer2.health == 0:
            return f"Кажется {self.gamer2.name} не в состоянии начать бой... {self.gamer1.name} побеждает!"

        # Тут начинается трэш для 15-ого ДЗ
        print(f"{self.gamer1}\n{self.gamer2}\n")
        print("НО! Произошла неведомая фигня!\n")
        self.gamer1.__dict__.update(health = self.gamer1.health + random.randint(-100, 100),
                                    attack = self.gamer1.attack + random.randint(-100, 100),
                                    defense = self.gamer1.defense + random.randint(-100, 100))
        self.gamer2.__dict__.update(health=self.gamer2.health + random.randint(-100, 100),
                                    attack=self.gamer2.attack + random.randint(-100, 100),
                                    defense=self.gamer2.defense + random.randint(-100, 100))
        print(f"{self.gamer1}\n{self.gamer2}\n")

        while True:

            # Возможность рандомно получить амуницию во время боя
            luck_gamer = random.randint(0, 10)
            if luck_gamer == 1:
                print(self.gamer1.put_on_gear(random.choice(self.list_ammunition)) + "\n")
            if luck_gamer == 2:
                print(self.gamer2.put_on_gear(random.choice(self.list_ammunition)) + "\n")

            print(self.gamer1.damage(self.gamer2) + "\n")
            if self.gamer2.health <= 0:
                return f"{self.gamer1.name} побеждает!"
            print(self.gamer2.damage(self.gamer1) + "\n")
            if self.gamer1.health <= 0:
                return f"{self.gamer2.name} побеждает!"


pers1 = Unit("PlayStation", 500, 150, 60)
pers2 = Unit("Xbox", 600, 100, 60)
pers3 = Unit("Nintendo", 200, 120, 50)
pers4 = Unit("PC", 2000, 150, 100)

amm1 = Ammunition("grafonium", 50, 30, 50)
amm2 = Ammunition("exclusive", -50, 70, 65)
amm3 = Ammunition("VR-mode", 25, 25, 50)
possible_ammunition = [amm1, amm2, amm3]

fight1 = Battle(pers1, pers2, *possible_ammunition)
print(fight1.fight())